import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Link from '@material-ui/core/Link';
import Container from '@material-ui/core/Container';
import StarIcon from '@material-ui/icons/StarBorder';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://moverslink.lt">
        Moverslink
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({

  mainHeader:{
    display: 'table',
    padding: theme.spacing(0, 0, 30),
    ...theme.typography.h2,
    fontFamily: '"IBM Plex Sans", sans-serif',
    ...theme.typography.h5,
    // eslint-disable-next-line
    fontFamily: '"IBM Plex Sans", sans-serif',
    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(0, 0, 15),
    },
  },
  appsContainer: {
    display: 'flex',
    [theme.breakpoints.down('sm')]: {
    flexDirection: 'column',
    alignItems: 'center',
    },
  },
  cardHeader:{
    backgroundColor: 'white'
  },
  heroContent: {
    padding: theme.spacing(6, 0, 12),
    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(3, 0, 6),
      // maxHeight: '120vh',
    },
  },
  toolbar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  toolbarTitle: {
    flex: 1,
  },
  toolbarSecondary: {
    justifyContent: 'space-between',
    overflowX: 'auto',
  },
  toolbarLink: {
    padding: theme.spacing(1),
    flexShrink: 0,
  },
  mainFeaturedPost1: {
    position: 'relative',
    backgroundColor: theme.palette.grey[180],
    color: theme.palette.common.white,
    marginBottom: theme.spacing(6),
    backgroundImage: 'url(https://miro.medium.com/max/12000/1*NaUmBti42n0FMMffyjTtBg.jpeg)',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
  },
  mainFeaturedPost2: {
    position: 'relative',
    backgroundColor: theme.palette.grey[180],
    color: theme.palette.common.white,
    marginBottom: theme.spacing(6),
    backgroundImage: 'url(https://tamebay.com/wp-content/uploads/2019/06/Customer-delivery-notifications.jpg)',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
  },
  mainFeaturedPost3: {
    position: 'relative',
    backgroundColor: theme.palette.grey[180],
    color: theme.palette.common.white,
    marginBottom: theme.spacing(6),
    backgroundImage: 'url(https://www.volvotrucks.com/content/dam/volvo/volvo-trucks/markets/global/splash-site/images/desktop-bg.jpg)',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
  },
  featuredPosts4Font:{
    textAlign: 'start',
    padding: theme.spacing(6),
    [theme.breakpoints.down('sm')]: {
      fontFamily:'"Roboto", "Helvetica", "Arial", sans-serif',
      fontWeight:'300',
      ...theme.typography.h2,
      padding: theme.spacing(0.5),
      fontSize:'6.6vw',
    },
    [theme.breakpoints.up('sm')]: {
      fontFamily:'"Roboto", "Helvetica", "Arial", sans-serif',
      fontWeight:'300',
      ...theme.typography.h2,
      align:'center',
      padding: theme.spacing(1),
      fontSize:'3rem',
    },
    [theme.breakpoints.up('md')]: {
      fontFamily:'"Roboto", "Helvetica", "Arial", sans-serif',
      fontWeight:'300',
      padding: theme.spacing(4),
      paddingRight: 0,
      ...theme.typography.h2,
      align:'center',
      fontSize:'4.5rem'
    }
  },
  mainFeaturedPost4: {
    position: 'relative',
    backgroundColor: theme.palette.grey[180],
    color: theme.palette.common.white,
    marginBottom: theme.spacing(6),
    backgroundImage: 'url(https://miro.medium.com/max/3840/1*-EJmEkkyqVqxs8W1xq4qog.jpeg)',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    h2:{
    [theme.breakpoints.up('sm')]: {
      padding: theme.spacing(6),
      paddingRight: 0,
      ...theme.typography.h2,
      fontSize:'1.5rem'
    },
    [theme.breakpoints.up('md')]: {
      padding: theme.spacing(6),
      paddingRight: 0,
      ...theme.typography.h2,
      fontSize:'1.75rem'
    },
  }
  },
  mainFeaturedPost5: {
    position: 'relative',
    backgroundColor: theme.palette.grey[180],
    color: theme.palette.common.white,
    marginBottom: theme.spacing(6),
    backgroundImage: 'url(https://digisolutionhub.com/wp-content/uploads/2018/05/Terms-Of-Service.jpg)',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center bottom',
  },
  overlay: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    backgroundColor: 'rgba(0,0,0,.4)',
  },
  iconStyle: {
    position: 'flex',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    backgroundColor: 'rgba(0,0,0,0)',
  },
  mainFeaturedPostContent: {
    position: 'relative',
    padding: theme.spacing(3),
    [theme.breakpoints.up('md')]: {
      padding: theme.spacing(6),
      paddingRight: 0,
    }
  },
  mainGrid: {
    marginTop: theme.spacing(3),
  },
  card: {
    display: 'flex',
  },
  cardDetails: {
    flex: 1,
  },
  cardMedia: {
    width: 160,
    height: 200
  },
  markdown: {
    ...theme.typography.body2,
    padding: theme.spacing(3, 0),
  },
  sidebarAboutBox: {
    padding: theme.spacing(2),
    backgroundColor: theme.palette.grey[200],
  },
  sidebarSection: {
    marginTop: theme.spacing(3),
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    marginTop: theme.spacing(8),
    padding: theme.spacing(6, 0),
  },
  headersFont:{
    fontFamily: '"IBM Plex Sans", sans-serif',
    fontWeight: '600',
    h1:{
      ...theme.typography.h1,
      fontFamily: '"IBM Plex Sans", sans-serif',
      fontSize: '1.5rem',
      fontWeight: '400',  
    },
    h2:{
      ...theme.typography.h2,
      fontFamily: '"IBM Plex Sans", sans-serif',
      fontSize: '1.5rem',
      fontWeight: '400',  
    },
    h5:{
      ...theme.typography.h5,
      fontFamily: '"IBM Plex Sans", sans-serif',
      fontSize: '5.5rem',
      fontWeight: '400',
    },  
    },
    qrCode: {
      [theme.breakpoints.down('sm')]: {
        display: 'none'
      }
    },
    buttonSize: {
      [theme.breakpoints.down('sm')]: {
        maxWidth: '250px',
        maxHeight: '160px',
        justifyContent: 'center',
        alignItems: 'center',
        margin: '5px',
      }
    },
}));

const appClient = [
  {
    title: 'Programa klientams',
    price: '0',
    description: ['10 users included', '2 GB of storage', 'Help center access', 'Email support'],
    buttonText: 'Sign up for free',
    buttonVariant: 'outlined',
  }
];

const appDriver = [
  {
    title: 'Programa vežėjams',
    price: '0',
    description: [
      '50 users included',
      '30 GB of storage',
      'Help center access',
      'Phone & email support',
    ],
    buttonText: 'Contact us',
    buttonVariant: 'outlined',
  }
];

const featuredPosts1 = [
  {
    title: 'KAIP TAI VEIKIA?',
    date: 'Moverslink',
    description:
      'Jei ieškote vežėjo perkraustymams, atskirų daiktų ar baldų pervežimui, automobilių ir motociklų pervežimams,pirmiausia parsisiųskite programėlę iš Google Play. Moverslink programėlėje užpildykite visus darbo duomenis ir rinkitės Jums patraukliausią pasiūlyma iš krūvos vežėjų.',
  }
];

const featuredPosts2 = [
    {
      title: 'KIEKVIENAM',
      date: 'Moverslink',
        description1:
        'Užpildykite paprastą pervežimo ar perkraustymo formą ir nuolatos gausite aktualią informaciją apie tolimesnę pekraustymo ar pervežimo eigą el. paštu ar sms žinute. ',
        description2:
        'Nereikia jokių telefono skambučių. Visos Moverslink sistemos  pristatymo ir perkraustymo įmonės yra vertinamos dėl jūsų ramybės. ',
        description3: 
        'Kad ir ką gabentumėte, mes stengiamės, kad Jūsų perkraustymas ar pervežimas būtų kuo lengvesnis ir ekonomiškesnis. ',
        description4: 
        'Mes bendradarbiaujame tik su pasirinktu transporto partnerių tinklu ir kiekvienam perkraustymui ar pervežimui nustatome aukščiausius standartus. ',
        description5: 
        'Kiekvienas naujas transporto partneris turi atitikti griežtus kriterijus, užtikrinančius, kad jis turi reikiamus draudimo ir licencijavimo dokumentus bei žinias. ',
        description6: 
        'Po kiekvieno darbo, mes prašome klientų atsiliepimų apie atliktą perkraustymą ar pervežimą ir naudojame šiuos balus, kad užtikrintume nuolat aukštą paslaugų kokybę. ',
        description7:
        'Reikia pagalbos? Rašykite mums el. Paštu arba skambinkite mums. Mes esame čia, kad padėtume.'
      }
  ];

  const featuredPosts3 = [
    {
      title: 'VEŽĖJAMS',
      date: 'Moverslink',
      description1:
      'Moverslink sistemoje Jūs galite siūlyti kainą Jums palankiems maršrutams. ',
      description2:
      'Galbūt Jūs išsikrovėte mieste, kur nėra atgalinio krovinio. Tuomet ši programėlė gali Jums padėti. ',
      description3:
      'Ieškokite perkraustymo, baldų, daiktų, automobilių pervežimo darbus ir negrįžkite tužčiomis. ',
      description4:
      'Jūs galite ieškoti darbus pagal miestą ar pagal patį darbo tipą. ',
      description5:
      'Moverslink sistema neturi jokio abonentinio mokesčio ir mokesčiai atsiranda tik tuomet kai užsakovas patvirtina Jūsų kainą, taip pasirinkdamas Jus kaip vežėją. ',
      description6:
      'Todėl jei esate patyręs vežėjas, savo srities profesionalas, susisiekite su mumis dėl registracijos mūsų sistemoje Moverslink.',
    }
  ];

  const featuredPosts4 = [
    {
      title: 'KONTAKTAI',
      date: 'Moverslink',
      description:
        'Reikia pagalbos? Rašykite mums el. paštu arba skambinkite mums. Mes esame čia, kad padėtume.',
    }
  ];

  const featuredPosts5 = [
    {
      title: 'TAISYKLĖS',
      date: 'TAISYKLĖS',
      description:
        'Su taisyklėmis galite susipažinti atsisiunte programėle.'
    }
  ];

export default function Blog() {
  const classes = useStyles();
  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxHeight="xl" maxWidth="xl">
        <main>

        <Container maxWidth="md" className={classes.mainHeader}>
          <Container maxWidth="md" component="main" className={classes.heroContent}>
          <Typography className={classes.headersFont} component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
              Moverslink
            </Typography>
            <Typography style={{ fontSize: '1.8rem'}} variant="h3" align="center" color="textSecondary" component="p">
              Sukurta taupyti jūsų brangų laiką
            </Typography>
          </Container>

          <Grid maxWidth="md" alignItems="flex-center">
          <Typography style={{ marginBottom: '30px', fontSize: '1.5rem'}} variant="h5" align="center" color="textPrimary" component="p">
            Moverslink pervežimo sistema yra nemokama, žemiau pasirinkite vieną iš programų:
            </Typography>

            <Grid alignItems="flex-center" className={classes.appsContainer}>
            {appClient.map(app => (
                <Grid item alignItems="flex-center" xs={12} sm={12} md={6}>
                  <Card className={classes.buttonSize} >
                    <CardHeader
                      title={app.title}
                      subheader={app.subheader}
                      titleTypographyProps={{ align: 'center' }}
                      subheaderTypographyProps={{ align: 'center' }}
                      action={app.title === 'Pro' ? <StarIcon /> : null}
                      className={app.cardHeader}
                    />
                    <CardContent>
                    <a href='https://play.google.com/store/apps/details?id=com.citycargo&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'>
                          <img
                          style={{ maxWidth: '170px', maxHeight: '170px'}}
                          alt='Get it on Google Play' 
                          src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png'/>
                      </a>
                      <CardContent className={classes.qrCode}>
                    {
                      <img
                      style={{ display: 'cover', maxWidth: '200px', maxHeight: '200px'}}
                        src="qrCodeDrivers.png"
                        alt="background"
                      />
                    }
                    </CardContent>
                    </CardContent>
                  </Card>
                </Grid>
              ))}

            {appDriver.map(app => (
                <Grid item alignItems="flex-center" xs={12} sm={12} md={6}>
                  <Card className={classes.buttonSize} >
                    <CardHeader
                      title={app.title}
                      subheader={app.subheader}
                      titleTypographyProps={{ align: 'center' }}
                      subheaderTypographyProps={{ align: 'center' }}
                      action={app.title === 'Pro' ? <StarIcon /> : null}
                      className={app.cardHeader}
                    />
                    <CardContent>
                    <a href='https://play.google.com/store/apps/details?id=com.moverslinkcar'>
                          <img
                          style={{ maxWidth: '170px', maxHeight: '170px'}}
                          alt='Get it on Google Play' 
                          src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png'/>
                      </a>
                      <CardContent className={classes.qrCode}>
                      {
                      <img
                      style={{ display: 'cover', maxWidth: '200px', maxHeight: '200px'}}
                      src="qrCodeClients.png"
                      alt="background"
                    />
                    }
                    </CardContent>
                    </CardContent>
                  </Card>
                </Grid>
              ))}
            </Grid>
         </Grid>
      </Container>

          <Grid container spacing={1} className={classes.cardGrid}>
              {featuredPosts1.map(post => (
                <Grid item key={post.title} xs={12} md={12}>
                  <CardActionArea component="a" /* href="#" */>
                    <Card className={classes.card}>
                      <div data-aos="zoom-in-left" className={classes.cardDetails}>
                        <CardContent>
                          <Typography className={classes.headersFont} component="h2" variant="h5">
                            {post.title}
                          </Typography>
                          <Typography variant="subtitle1" paragraph>
                            {post.description}
                          </Typography>
                        </CardContent>
                      </div>
                    </Card>
                  </CardActionArea>
                </Grid>
              ))}
            </Grid>

            {/* Main featured post */}
            <Paper className={classes.mainFeaturedPost1}>
              {/* Increase the priority of the hero background image */}
              {
                <img
                  style={{ display: 'none' }}
                  src="https://cdn.simplesite.com/i/3e/ad/285697108421946686/i285697114409099280._szw1280h1280_.jpg"
                  alt="background"
                />
              }
              <div data-aos="zoom-in-left" className={classes.overlay} />
              <Grid container>
                <Grid item md={6}>
                  <div className={classes.mainFeaturedPostContent}>
                  <Typography component="h5" color="inherit" align="center" className={classes.featuredPosts4Font} gutterBottom>
                      Veikimas šešiais žingsniais
                    </Typography>
                  </div>
                </Grid>
              </Grid>
            </Paper>
            {/* End main featured post */}


            {/* Sub featured posts */}
            <Grid container spacing={1} className={classes.cardGrid}>
              {featuredPosts2.map(post => (
                <Grid item key={post.title} xs={12} md={12}>
                  <CardActionArea component="a" /* href="#" */>
                    <Card className={classes.card}>
                      <div className={classes.cardDetails}>
                        <CardContent>
                          <Typography className={classes.headersFont} component="h2" variant="h5">
                            {post.title}
                          </Typography>
                          <Typography align="center" variant="subtitle1" paragraph>
                          {post.description}
                          {post.description1}
                          {post.description2}
                          {post.description3}
                          {post.description4}
                          {post.description5}
                          {post.description6}
                          {post.description7}
                          </Typography>
                        </CardContent>
                      </div>
                    </Card>
                  </CardActionArea>
                </Grid>
              ))}
            </Grid>

            <Paper className={classes.mainFeaturedPost2}>
              {/* Increase the priority of the hero background image */}
              {
                <img
                  style={{ display: 'none' }}
                  src="https://tamebay.com/wp-content/uploads/2019/06/Customer-delivery-notifications.jpg"
                  alt="background"
                />
              }
              <div className={classes.overlay} />
              <Grid container>
                <Grid item md={6}>
                  <div className={classes.mainFeaturedPostContent}>
                  <Typography component="h5" color="inherit" align="center" className={classes.featuredPosts4Font} gutterBottom>
                      Klientą ir vežėją jungianti platforma
                    </Typography>
                  </div>
                </Grid>
              </Grid>
            </Paper>

                        {/* Sub featured posts */}
                        <Grid container spacing={1} className={classes.cardGrid}>
              {featuredPosts3.map(post => (
                <Grid item key={post.title} xs={12} md={12}>
                  <CardActionArea component="a" /* href="#" */>
                    <Card className={classes.card}>
                      <div className={classes.cardDetails}>
                        <CardContent>
                          <Typography className={classes.headersFont} component="h2" variant="h5">
                            {post.title}
                          </Typography>
                          <Typography align="center" variant="subtitle1" paragraph>
                          {post.description}
                          {post.description1}
                          {post.description2}
                          {post.description3}
                          {post.description4}
                          {post.description5}
                          {post.description6}
                          {post.description7}
                          </Typography>
                        </CardContent>
                      </div>
                    </Card>
                  </CardActionArea>
                </Grid>
              ))}
            </Grid>


            <Paper className={classes.mainFeaturedPost3}>
              {/* Increase the priority of the hero background image */}
              {
                <img
                  style={{ display: 'none' }}
                  src="https://www.volvotrucks.com/content/dam/volvo/volvo-trucks/markets/global/splash-site/images/desktop-bg.jpg"
                  alt="background"
                />
              }
              <div className={classes.overlay} />
              <Grid container>
                <Grid item md={6}>
                  <div className={classes.mainFeaturedPostContent}>
                  <Typography component="h5" color="inherit" align="center" className={classes.featuredPosts4Font} gutterBottom>
                      Dideliems bei mažiems vežėjams
                    </Typography>
                  </div>
                </Grid>
              </Grid>
            </Paper>


            <Grid container spacing={1} className={classes.cardGrid}>
              {featuredPosts4.map(post => (
                <Grid item key={post.title} xs={12} md={12}>
                  <CardActionArea component="a" /* href="#" */>
                    <Card className={classes.card}>
                      <div className={classes.cardDetails}>
                        <CardContent>
                          <Typography className={classes.headersFont} component="h2" variant="h5">
                            {post.title}
                          </Typography>
                          <Typography variant="subtitle1" paragraph>
                            {post.description}
                          </Typography>
                        </CardContent>
                      </div>
                    </Card>
                  </CardActionArea>
                </Grid>
              ))}
            </Grid>

            <Paper className={classes.mainFeaturedPost4}>
              {/* Increase the priority of the hero background image */}
              {
                <img
                  style={{ display: 'none' }}
                  src="https://cdn.simplesite.com/i/3e/ad/285697108421946686/i285697114409096324._szw1280h1280_.jpg"
                  alt="background"
                />
              }
              <div className={classes.overlay} />
              <Grid container>
                <Grid item md={10}>
                  <div className={classes.mainFeaturedPostContent}>
                    <Typography component="h5" color="inherit" align="center" className={classes.featuredPosts4Font} gutterBottom>
                      algirdas@moverslink.lt
                      +370 609 60824
                    </Typography>
                  </div>
                </Grid>
              </Grid>
            </Paper>


            <Grid container spacing={1} className={classes.cardGrid}>
              {featuredPosts5.map(post => (
                <Grid item key={post.title} xs={12} md={12}>
                  <CardActionArea component="a" /* href="#" */>
                    <Card className={classes.card}>
                      <div className={classes.cardDetails}>
                        <CardContent>
                          <Typography className={classes.headersFont} component="h2" variant="h5">
                            {post.title}
                          </Typography>
                          <Typography variant="subtitle1" paragraph>
                            {post.description}
                          </Typography>
                        </CardContent>
                      </div>
                    </Card>
                  </CardActionArea>
                </Grid>
              ))}
            </Grid>

            <Paper className={classes.mainFeaturedPost5}>
              {/* Increase the priority of the hero background image */}
              {
                <img
                  style={{ display: 'none' }}
                  src="https://digisolutionhub.com/wp-content/uploads/2018/05/Terms-Of-Service.jpg"
                  alt="background"
                />
              }
              <div className={classes.overlay} />
              <Grid container>
                <Grid item md={6}>
                  <div className={classes.mainFeaturedPostContent}>
                  <Typography component="h5" color="inherit" align="center" className={classes.featuredPosts4Font} gutterBottom>
                      Nevaržančios naudojimosi taisyklės
                    </Typography>
                  </div>
                </Grid>
              </Grid>
            </Paper>
        </main>
      </Container>
      {/* Footer */}
      <footer className={classes.footer}>
        <Container maxWidth="xl">
          <Copyright />
        </Container>
      </footer>
      {/* End footer */}
    </React.Fragment>
  );
}