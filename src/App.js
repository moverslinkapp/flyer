import React from 'react';
import './App.css';
import Container from '@material-ui/core/Container';
import Blog from './Blog';

function App() {
  return (
    <div className="App">
      <Container fixed>
        <Blog />
      </Container>
    </div>
  );
}

export default App;